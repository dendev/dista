<?php


use Dendev\Dista\Http\Controllers\DistaController;
use Illuminate\Support\Facades\Route;

Route::middleware(['web'])->prefix('dista')->group(function () {
    Route::post('/sandbox/reset', [DistaController::class, 'sandbox_reset'])->name('dista.sanbox.reset');
    Route::post('/sandbox/resetone', [DistaController::class, 'sandbox_resetone'])->name('dista.sanbox.resetone');
    Route::get('/sandbox/{matrix_type}', [DistaController::class, 'sandbox'])->name('dista.sanbox');
    Route::post('/execute', [DistaController::class, 'execute'])->name('dista.execute');
});
