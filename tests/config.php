<?php

return [
    'db' => [
        'sheldon' => [
            'driver' => 'pgsql',
            'url' => '',
            'host' => '127.0.0.1',
            'port' => '5432',
            'database' => 'sheldon',
            'username' => 'dendev',
            'password' => 'dendev',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'brain',
            'sslmode' => 'prefer',
        ],
        'mysql' => [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => 3306,
            'database' => 'larapack_test',
            'username' => 'larapack',
            'password' => 'larapack',
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],
    ],
];
