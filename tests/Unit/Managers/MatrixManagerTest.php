<?php

namespace Tests\Unit\Managers;

use Orchestra\Testbench\TestCase;

class MatrixManagerTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Dista\DistaServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        /*
        $connection = $config['db']['sheldon'];

        $app['config']->set('database.default', 'sheldon');
        $app['config']->set('database.connections.sheldon', $connection);
        */
    }

    public function testMe()
    {
        $this->assertEquals('matrix_manager', \MatrixManager::test_me());
    }
}

