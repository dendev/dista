@if( array_key_exists('event', $experiment['inputs']))
    <div class="field column">
        <label class="label" for="events">Event</label>
        <div class="control" id="events">
            <span class="tag">{{$experiment['inputs']['event']->label}}</span>
        </div>
    </div>
@endif
