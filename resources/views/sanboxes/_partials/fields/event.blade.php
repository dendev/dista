@if( array_key_exists('events', $experiment['options']))
    <div class="field column">
        <label class="label" for="events">Available events</label>
        <div class="control" id="events">
            @if( count($experiment['options']['events']) > 0)
                @foreach($experiment['options']['events'] as $event)
                    <span class="tag">{{$event->label}}</span>
                @endforeach
            @else
                <span> - </span>
            @endif
        </div>
    </div>
@endif
