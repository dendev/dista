@if( array_key_exists('workers', $experiment['options']) )
<div class="field column">
    <label class="label" for="worker-id">Worker</label>
    <div class="control">
        <div class="select is-fullwidth">
            <select name="worker_id" id="worker-id" class="w-100" @if( $type !== 'state' ) disabled @endif >
                @foreach($experiment['options']['workers'] as $worker)
                    <option value="{{$worker->id}}"
                            @if(array_key_exists('worker', $experiment['inputs']) && $experiment['inputs']['worker']->id == $worker->id) selected @endif
                    >
                        {{$worker->label}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
@endif
