@if( array_key_exists('resulting_state', $experiment['inputs']))
    <div class="field column">
        <label class="label" for="resulting_state">Resulting state</label>
        <div class="control" id="resulting_state">
            <span class="tag">{{$experiment['inputs']['resulting_state']->label}}</span>
        </div>
    </div>
@endif
