@if( array_key_exists('initial_states', $experiment['options']) )
    <div class="field column">
        <label class="label" for="initial-state">Initial state</label>
        <div class="control">
            <div class="select is-fullwidth">
                <select name="initial_state_id" id="initial-state" class="w-100" @if( $type !== 'state' ) disabled @endif >
                    @foreach($experiment['options']['initial_states'] as $initial_state)
                        <option value="{{$initial_state->id}}"
                                @if( (array_key_exists('initial_state', $experiment['inputs'] ) && $experiment['inputs']['initial_state']->id == $initial_state->id) )
                                    selected
                            @endif
                        >
                            {{$initial_state->label}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
@endif
