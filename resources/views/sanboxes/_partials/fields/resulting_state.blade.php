@if( array_key_exists('transition', $experiment['inputs']))
    <div class="field column">
        <label class="label" for="transition">Transition</label>
        <div class="control" id="transition">
            <span class="tag">{{$experiment['inputs']['transition']->label}}</span>
        </div>
    </div>
@endif
