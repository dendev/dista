@if( $type === 'state' )
    <h3 class="has-text-centered">What are the possible actions ?</h3>

    <div class="control has-text-centered">
        <button type="submit" class="button is-info">
            <span class="icon">
                <i class="fa-solid fa-magnifying-glass"> </i>
            </span>
            <span>Check</span>
        </button>
    </div>
@endif

