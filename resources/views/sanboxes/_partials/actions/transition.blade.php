@if( $type === 'transition' )
    <h3 class="has-text-centered">Run transition</h3>

    <div class="control has-text-centered">
        <button type="submit" class="button is-info">
            <span class="icon">
                <i class="fa-solid fa-cog"> </i>
            </span>
            <input type="hidden" name="transition_id"
                   value="{{$experiment['options']['transition']->id}}">
            <span>{{$experiment['options']['transition']->label}}</span>
        </button>
    </div>
@endif
