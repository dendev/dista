@if( $type === 'event' )
    <!-- events -->
    <h3 class="has-text-centered">At this state worker can </h3>

    <div class="control has-text-centered">
        @if( count( $experiment['options']['events'] ) > 0 )
            @foreach($experiment['options']['events'] as $event)
                <button type="submit" class="button is-info is-outlined">
                    <input type="hidden" name="event_id" value="{{$event->id}}">
                    <span>{{$event->label}}</span>
                </button>
            @endforeach
        @else
            <button type="submit" class="button is-danger is-outlined disabled" disabled>
                <span>do </span>
                <i class="fa-solid fa-ban ml-2"> </i>
            </button>
        @endif
    </div>
@endif
