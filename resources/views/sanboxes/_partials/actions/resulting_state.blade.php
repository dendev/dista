@if( $type === 'resulting_state' )
    <h3 class="has-text-centered">Resulting state</h3>

    <div class="control has-text-centered">
        <button type="submit" class="button is-info">
            <span class="icon">
                <i class="fa-solid fa-flag"> </i>
            </span>
            <input type="hidden" name="resulting_state_id"
                   value="{{$experiment['options']['resulting_state']->id}}">
            <span>{{$experiment['options']['resulting_state']->label}}</span>
        </button>
    </div>
@endif
