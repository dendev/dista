@extends('dendev::layouts.sandbox')

@section('content')
    <!-- reset -->
    <section>
        <div class="column is-2 is-offset-8">
            <form action="{{route('dista.sanbox.reset')}}" method="post">
                @csrf

                <input type="hidden" name="matrix_type" value="{{$matrix_type->identity}}">

                <div class="has-text-right">
                    <button type="submit" class="button is-danger">
                    <span class="icon">
                        <i class="fa-solid fa-arrow-rotate-right"></i>
                    </span>
                        <span>Reset</span>
                    </button>
                </div>
            </form>
        </div>
    </section>

    <!-- explorer -->
    <section class="articles">
        <div class="column is-8 is-offset-2">
            @foreach( $experiments as $key => $experiment )
            <div class="card article mb-4">
                <div class="card-content">

                    @if( $experiment['is_last'] )
                    <!-- reset -->
                    <div class="media-content has-text-right">
                        <form action="{{route('dista.sanbox.resetone')}}" method="post">
                            @csrf
                            <input type="hidden" name="matrix_type" value="{{$matrix_type->identity}}">
                            <div class="has-text-right">
                                <button type="submit" class="button is-warning">
                                    <span class="icon">
                                        <i class="fa-solid fa-arrow-rotate-right"></i>
                                    </span>
                                    <span>Reset</span>
                                </button>
                            </div>
                        </form>
                    </div>
                    @endif

                    <!-- title -->
                    <div class="media" id="title">
                        <div class="media-content has-text-centered">
                            <p class="title article-title" >Explorer {{$key}}</p>
                            <div class="tags has-addons level-item">
                                <span class="tag is-rounded is-info">{{$experiment['inputs']['initial_state']->label}}</span>
                                <span class="tag is-rounded">{{$experiment['type']}}</span>
                            </div>
                        </div>
                    </div>

                    @if( $experiment['has_focus'] )
                        <div id="focus"></div>
                    @endif

                    <div class="content article-body">
                        <!-- infos -->
                        <p>{{$experiment['inputs']['initial_state']->description}}</p>

                        <!-- settings -->
                        <form action="{{route('dista.execute')}}" method="post">
                            @csrf

                            @php $type = $experiment['type'] @endphp

                            @include('dendev::sanboxes._partials.fields.initial_state', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.fields.worker', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.fields.event', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.fields.transition', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.fields.resulting_state', [
                                'type' => $type,
                                'experiment' => $experiment,
                           ])

                            @include('dendev::sanboxes._partials.fields.done', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            <!-- actions -->
                            @include('dendev::sanboxes._partials.actions.state', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.actions.event', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.actions.transition', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])

                            @include('dendev::sanboxes._partials.actions.resulting_state', [
                                'type' => $type,
                                'experiment' => $experiment,
                            ])


                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>
@endsection
