<section class="hero is-info is-medium is-bold mb-5">
    <div class="hero-body">
        <div class="container has-text-centered">
            <h1 class="title">{{$matrix_type->label}}</h1>
            <h2 class="title">{{$matrix_type->description}}</h2>
        </div>
    </div>
</section>
