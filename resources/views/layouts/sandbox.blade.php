<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dista sandbox</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('vendor/dendev/imgs/favicon/favicon.ico')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Bulma Version 0.9.X-->
    <link rel="stylesheet" href="https://unpkg.com/bulma@0.9.4/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/dendev/css/dista.css')}}">
</head>

<body>
@include('dendev::layouts._partials.navbar')

@include('dendev::layouts._partials.hero', ['matrix_type' => $matrix_type])

<div class="container">
    @yield('content')
</div>

</body>

</html>
