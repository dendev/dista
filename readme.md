# Dista

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

Via Composer

```bash
composer require dendev/dista
```

## Usage

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

```bash
composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email author@email.com instead of using the issue tracker.

## TODO 
add rules on state ( in, out ) who use hoa ruler 

## Credits

- [Jeroen-G for is packager skeleton](https://github.com/jeroen-g/packager-skeleton)
- [Author Name][link-author]
- [All Contributors][link-contributors]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/dendev/dista.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/dendev/dista.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/dendev/dista/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/dendev/dista
[link-downloads]: https://packagist.org/packages/dendev/dista
[link-travis]: https://travis-ci.org/dendev/dista
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/dendev
[link-contributors]: ../../contributors

## Refs
