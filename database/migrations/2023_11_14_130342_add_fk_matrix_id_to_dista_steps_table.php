<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('dista_steps', function (Blueprint $table) {
            $table->foreign('matrix_id')->references('id')->on('dista_matrices');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('dista_steps', function (Blueprint $table) {
            $table->dropForeign(['matrix_id']);
        });
    }
};
