<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dista_files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('workflow_id')->constrained('dista_workflows');
            $table->string('label');
            $table->string('path');
            $table->foreignId('worker_id')->constrained('dista_workers');
            $table->dateTime('at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dista_files');
    }
};
