<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dista_activities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('matrix_id')->constrained('dista_matrices');
            $table->foreignId('type_id')->constrained('dista_activity_types');
            $table->string('table_name');
            $table->string('table_id');
            $table->boolean('is_at_begin')->nullable();
            $table->boolean('is_at_end')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dista_activities');
    }
};
