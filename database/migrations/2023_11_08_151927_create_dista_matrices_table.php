<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dista_matrices', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->string('identity');
            $table->foreignId('type_id')->constrained('dista_matrix_types');
            $table->foreignId('step_id')->nullable()->constrained('dista_steps');
            $table->foreignId('initial_state_id')->constrained('dista_states');
            $table->foreignId('event_id')->constrained('dista_events');
            $table->foreignId('transition_id')->constrained('dista_transitions');
            $table->foreignId('resulting_state_id')->constrained('dista_states');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dista_matrices');
    }
};
