<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('dista_event_worker_permissions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('worker_id')->constrained('dista_workers');
            $table->foreignId('event_id')->constrained('dista_events');
            $table->string('label');
            $table->string('identity');
            $table->text('description')->nullable();
            $table->unsignedInteger('counter')->nullable();
            $table->dateTime('begin_at')->nullable();
            $table->dateTime('end_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('dista_event_worker_permissions');
    }
};
