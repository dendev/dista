<?php

namespace Dendev\Dista\Database\Seeders;

use Dendev\Dista\Models\WorkflowType;
use Illuminate\Database\Seeder;

class MatrixTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            //['label' => '', 'identity' => '', 'description' => ''],
            ['label' => 'Publishing', 'identity' => 'publishing', 'description' => ''],
        ];

        foreach( $datas as $data )
        {
            $state_type = new MatrixType($data);
            $state_type->save();
        }
    }
}
