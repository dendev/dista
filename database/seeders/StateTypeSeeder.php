<?php

namespace Dendev\Dista\Database\Seeders;

use Dendev\Dista\Models\StateType;
use Illuminate\Database\Seeder;

class StateTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            //['label' => '', 'identity' => '', 'description' => ''],
            ['label' => 'Start', 'identity' => 'start', 'description' => 'Should only be one per process. This state is the state into which a new Request is placed when it is created.'],
            ['label' => 'Normal', 'identity' => 'normal', 'description' => 'A regular state with no special designation'],
            ['label' => 'Complete', 'identity' => 'complete', 'description' => 'A state signifying that any Request in this state have completed normally.'],
            ['label' => 'Denied', 'identity' => 'denied', 'description' => 'A state signifying that any Request in this state has been denied (e.g. never got started and will not be worked on).'],
            ['label' => 'Cancelled', 'identity' => 'cancelled', 'description' => 'A state signifying that any Request in this state has been cancelled'],
        ];

        foreach( $datas as $data )
        {
            $state_type = new StateType($data);
            $state_type->save();
        }
    }
}
