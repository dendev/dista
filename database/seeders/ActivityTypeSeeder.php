<?php

namespace Dendev\Dista\Database\Seeders;

use Dendev\Dista\Models\ActivityType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            //['label' => '', 'identity' => '', 'description' => ''],
            ['label' => 'Add Note', 'identity' => 'add_note', 'description' => 'Specifies that we should automatically add a note to a Request.'],
            ['label' => 'Send Email', 'identity' => 'send_email', 'description' => 'Specifies that we should send an email to one or more recipients.'],
            ['label' => 'Add Stackholders', 'identity' => 'add_stackholder', 'description' => 'Specifies that we should add one or more persons as Stakeholders on this request.'],
            ['label' => 'Remove Stackholders', 'identity' => 'remove_stackholder', 'description' => 'Specifies that we should remove one or more stakeholders from this request.'],
        ];

        foreach( $datas as $data )
        {
            $activity_type = new ActivityType($data);
            $activity_type->save();
        }
    }
}
