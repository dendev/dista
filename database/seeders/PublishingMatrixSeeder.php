<?php

namespace Dendev\Dista\Database\Seeders;

use Dendev\Dista\Models\ActivityType;
use Dendev\Dista\Models\Event;
use Dendev\Dista\Models\EventWorkerPermission;
use Dendev\Dista\Models\Matrix;
use Dendev\Dista\Models\MatrixType;
use Dendev\Dista\Models\State;
use Dendev\Dista\Models\StateType;
use Dendev\Dista\Models\Transition;
use Dendev\Dista\Models\Worker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PublishingMatrixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            'matrix_types' => ['label' => 'Publishing', 'identity' => 'publishing', 'description' => ''],
            'state_types' => [
                ['label' => 'first', 'identity' => 'first']
            ],
            'states' => [
                ['type_identity' => 'first', 'label' => 'Empty', 'identity' => 'empty'],
                ['type_identity' => 'first', 'label' => 'Draft', 'identity' => 'draft', 'description' => 'Authors create content drafts. These drafts could be articles, blog posts, reports, etc. Content at this stage is typically not visible to the public.'],
                ['type_identity' => 'first', 'label' => 'Review', 'identity' => 'review', 'description' => 'Drafts undergo a review process. Reviewers, who may be editors, content managers, or other stakeholders, examine the content to ensure its quality, relevance, and compliance with editorial standards.'],
                ['type_identity' => 'first', 'label' => 'Review rejected', 'identity' => 'review_rejected', 'description' => ''],
                ['type_identity' => 'first', 'label' => 'Draft edition', 'identity' => 'draft_edit', 'description' => ''],
                ['type_identity' => 'first', 'label' => 'Review accepted', 'identity' => 'review_accepted', 'description' => ''],
                ['type_identity' => 'first', 'label' => 'Approval', 'identity' => 'approval', 'description' => 'After review, content may require approval before being published. Authorized individuals, such as editorial managers, can give the green light to publish the content.'],
                ['type_identity' => 'first', 'label' => 'Approval rejected', 'identity' => 'approval_rejected', 'description' => ''],
                ['type_identity' => 'first', 'label' => 'Approval accepted', 'identity' => 'approval_accepted', 'description' => ''],
                ['type_identity' => 'first', 'label' => 'Scheduling', 'identity' => 'scheduling', 'description' => 'Approved content can be scheduled for publication at a specific date and time.'],
                ['type_identity' => 'first', 'label' => 'Scheduling rejected', 'identity' => 'scheduling_rejected', 'description' => ''],
                ['type_identity' => 'first', 'label' => 'Scheduling accepted', 'identity' => 'scheduling_accepted', 'description' => ''],
                //['label' => 'Monitoring', 'identity' => 'monitoring', 'description' => 'Uuser reactions can be monitored'],
                ['type_identity' => 'first', 'label' => 'Publishing', 'identity' => 'publishing', 'description' => 'The content is published and becomes accessible to the public. It can be disseminated on the website, social media, or other defined distribution channels.'],
                ['type_identity' => 'first', 'label' => 'Unpublishing', 'identity' => 'unpublishing', 'description' => 'The content is unpublished and becomes inaccessible to the public.'],
                ['type_identity' => 'first', 'label' => 'Promotion', 'identity' => 'promotion', 'description' => 'If necessary, content may be actively promoted post-publication. This could involve marketing strategies, social media promotion campaigns, or other initiatives aimed at increasing visibility'],
                ['type_identity' => 'first', 'label' => 'Feedback', 'identity' => 'feedback', 'description' => 'feedback can help assess content performance and inform future editorial decisions.'],
                ['type_identity' => 'first', 'label' => 'Archiving', 'identity' => 'archive', 'description' => 'After publication, content may move to the archiving stage. This may include creating backups, archiving previous versions, or other processes for managing editorial history.'],
            ],
            'transitions' => [
                ['label' => 'Empty To Draft', 'identity' => 'empty_to_draft'],
                ['label' => 'Draft To Review', 'identity' => 'draft_to_review'],
                ['label' => 'Review to Reject', 'identity' => 'review_to_rejected'],
                ['label' => 'Reject to Edit', 'identity' => 'rejected_to_draft'],
                ['label' => 'Review to Accepted', 'identity' => 'review_to_accepted'],
                ['label' => 'Review Accepted To Approval', 'identity' => 'accepted_to_approval'],
                ['label' => 'Approval To Reject', 'identity' => 'approval_to_rejected'],
                ['label' => 'Approval To Accepted', 'identity' => 'approval_to_accepted'],
                ['label' => 'Approval To Scheduling', 'identity' => 'approval_to_scheduling'],
                ['label' => 'Scheduling rejected', 'identity' => 'scheduling_to_rejected'],
                ['label' => 'Scheduling accepted', 'identity' => 'scheduling_to_accepted'],
                ['label' => 'Scheduling To Publishing', 'identity' => 'scheduling_to_publishing'],
                ['label' => 'Publishing To Promotion', 'identity' => 'publishing_to_promotion'],
                ['label' => 'Promotion To publishing', 'identity' => 'promotion_to_publishing'],
                ['label' => 'Publishing To Unpublishing', 'identity' => 'publishing_to_unpublishing'],
                ['label' => 'Unpublishing To Feedback', 'identity' => 'unpublishing_to_feedback'],
                ['label' => 'Feedback To Archiving', 'identity' => 'feedback_to_archiving'],
            ],
            'events' => [
                ['label' => 'Create', 'identity' => 'create_draft'],
                ['label' => 'Submit review', 'identity' => 'submit_to_review'],
                ['label' => 'Review reject', 'identity' => 'review_reject'],
                ['label' => 'Edit draft', 'identity' => 'edit_draft'],
                ['label' => 'Valid', 'identity' => 'review_accept'],
                ['label' => 'Submit approver', 'identity' => 'submit_to_approval'],
                ['label' => 'Approval reject', 'identity' => 'approval_reject'],
                ['label' => 'Approval accept', 'identity' => 'approval_accept'],
                ['label' => 'Submit scheduling', 'identity' => 'submit_to_scheduling'],
                ['label' => 'Scheduling reject', 'identity' => 'scheduling_reject'],
                ['label' => 'Scheduling accept', 'identity' => 'scheduling_accept'],
                ['label' => 'Publish', 'identity' => 'publish'],
                ['label' => 'Promote', 'identity' => 'promote'],
                ['label' => 'Unpromote', 'identity' => 'unpromote'],
                ['label' => 'Unpublish', 'identity' => 'unpublish'],
                ['label' => 'Feedback', 'identity' => 'feedback'],
                ['label' => 'Archiving', 'identity' => 'archiving'],
                //['label' => '', 'identity' => 'to_review'],
            ],
            'workers' => [
                ['label' => 'Author', 'identity' => 'author'],
                ['label' => 'Editor', 'identity' => 'editor'],
                ['label' => 'Content Manager', 'identity' => 'content_manager'],
                ['label' => 'Approver', 'identity' => 'approver'],
                ['label' => 'Content scheduler', 'identity' => 'content_scheduler'],
                ['label' => 'Promoter', 'identity' => 'promoter'],
                ['label' => 'Analyst', 'identity' => 'analyst'],
                ['label' => 'Archivist', 'identity' => 'archivist'],
            ],
            'event_worker_permissions' => [
                ['worker_identity' => 'author', 'event_identity' => 'create_draft', 'label' => 'Can create content', 'identity' => 'can_create_content'],
                ['worker_identity' => 'author', 'event_identity' => 'submit_to_review', 'label' => 'Can submit to review', 'identity' => 'can_submit_to_review'],

                ['worker_identity' => 'author', 'event_identity' => 'edit_draft', 'label' => 'Edit draft', 'identity' => 'can_edit_draft_by_author'],
                ['worker_identity' => 'editor', 'event_identity' => 'review_reject', 'label' => 'Reject', 'identity' => 'can_review_reject'],
                ['worker_identity' => 'editor', 'event_identity' => 'review_accept', 'label' => 'Accept', 'identity' => 'can_review_accept'],
                ['worker_identity' => 'editor', 'event_identity' => 'submit_to_approval', 'label' => 'Submit approval', 'identity' => 'can_submit_to_approval'],

                ['worker_identity' => 'approver', 'event_identity' => 'approval_reject', 'label' => 'Approval rejected', 'identity' => 'can_reject_approval'],
                ['worker_identity' => 'approver', 'event_identity' => 'approval_accept', 'label' => 'Approval accepted', 'identity' => 'can_accept_approval'],
                ['worker_identity' => 'approver', 'event_identity' => 'submit_to_scheduling', 'label' => 'Submit scheduling', 'identity' => 'can_submit_to_scheduling'],

                ['worker_identity' => 'content_scheduler', 'event_identity' => 'scheduling_reject', 'label' => 'Reject scheduling', 'identity' => 'can_reject_scheduling'],
                ['worker_identity' => 'content_scheduler', 'event_identity' => 'scheduling_accept', 'label' => 'Accept scheduling', 'identity' => 'can_accept_scheduling'],
                ['worker_identity' => 'content_scheduler', 'event_identity' => 'publish', 'label' => 'Publish', 'identity' => 'can_publish'],
                ['worker_identity' => 'content_scheduler', 'event_identity' => 'unpublish', 'label' => 'Unpublish', 'identity' => 'can_unpublish'],
                // TODO reject === content_scheduler can edit dates -> edited dates to scheduling

                ['worker_identity' => 'promoter', 'event_identity' => 'promote', 'label' => 'Promote', 'identity' => 'can_promote'],
                ['worker_identity' => 'promoter', 'event_identity' => 'unpromote', 'label' => 'Unpromote', 'identity' => 'can_unpromote'],

                ['worker_identity' => 'analyst', 'event_identity' => 'feedback', 'label' => 'Make feedback', 'identity' => 'can_feedback'],

                ['worker_identity' => 'archivist', 'event_identity' => 'archiving', 'label' => 'Archiving', 'identity' => 'can_archive'],
            ],
            'matrix' => [
                ['type_identity' => 'publishing', 'label' => 'Create', 'identity' => 'create', 'initial_state_identity' => 'empty', 'event_identity' => 'create_draft', 'transition_identity' => 'empty_to_draft', 'resulting_state_identity' => 'draft' ],
                ['type_identity' => 'publishing', 'label' => 'Review', 'identity' => 'review', 'initial_state_identity' => 'draft', 'event_identity' => 'submit_to_review', 'transition_identity' => 'draft_to_review', 'resulting_state_identity' => 'review' ],
                ['type_identity' => 'publishing', 'label' => 'Review rejected', 'identity' => 'review_rejected', 'initial_state_identity' => 'review', 'event_identity' => 'review_reject', 'transition_identity' => 'review_to_rejected', 'resulting_state_identity' => 'review_rejected' ],
                ['type_identity' => 'publishing', 'label' => 'Edit draft', 'identity' => 'edit_draft', 'initial_state_identity' => 'review_rejected', 'event_identity' => 'edit_draft', 'transition_identity' => 'rejected_to_draft', 'resulting_state_identity' => 'draft' ],
                ['type_identity' => 'publishing', 'label' => 'Draft edited', 'identity' => 'draft_edited', 'initial_state_identity' => 'draft_edit', 'event_identity' => 'submit_to_review', 'transition_identity' => 'draft_to_review', 'resulting_state_identity' => 'review' ],
                ['type_identity' => 'publishing', 'label' => 'Review accepted', 'identity' => 'review_accepted', 'initial_state_identity' => 'review', 'event_identity' => 'review_accept', 'transition_identity' => 'review_to_accepted', 'resulting_state_identity' => 'review_accepted' ],

                ['type_identity' => 'publishing', 'label' => 'Approval', 'identity' => 'approval', 'initial_state_identity' => 'review_accepted', 'event_identity' => 'submit_to_approval', 'transition_identity' => 'accepted_to_approval', 'resulting_state_identity' => 'approval' ],
                ['type_identity' => 'publishing', 'label' => 'Approval rejected', 'identity' => 'approval_rejected', 'initial_state_identity' => 'approval', 'event_identity' => 'approval_reject', 'transition_identity' => 'approval_to_rejected', 'resulting_state_identity' => 'approval_rejected' ],
                ['type_identity' => 'publishing', 'label' => 'Approval accepted', 'identity' => 'approval_accepted', 'initial_state_identity' => 'approval', 'event_identity' => 'approval_accept', 'transition_identity' => 'approval_to_accepted', 'resulting_state_identity' => 'approval_accepted' ],

                ['type_identity' => 'publishing', 'label' => 'Scheduling', 'identity' => 'scheduling', 'initial_state_identity' => 'approval_accepted', 'event_identity' => 'submit_to_scheduling', 'transition_identity' => 'approval_to_scheduling', 'resulting_state_identity' => 'scheduling' ],
                ['type_identity' => 'publishing', 'label' => 'Scheduling rejected', 'identity' => 'scheduling_rejected', 'initial_state_identity' => 'scheduling', 'event_identity' => 'scheduling_reject', 'transition_identity' => 'scheduling_to_rejected', 'resulting_state_identity' => 'scheduling_rejected' ],
                ['type_identity' => 'publishing', 'label' => 'Scheduling accepted', 'identity' => 'scheduling_accepted', 'initial_state_identity' => 'scheduling', 'event_identity' => 'scheduling_accept', 'transition_identity' => 'scheduling_to_accepted', 'resulting_state_identity' => 'scheduling_accepted' ],
                ['type_identity' => 'publishing', 'label' => 'Scheduling Publishing', 'identity' => 'scheduling_publishing', 'initial_state_identity' => 'scheduling_accepted', 'event_identity' => 'publish', 'transition_identity' => 'scheduling_to_publishing', 'resulting_state_identity' => 'publishing' ],

                ['type_identity' => 'publishing', 'label' => 'Promote', 'identity' => 'promote', 'initial_state_identity' => 'publishing', 'event_identity' => 'promote', 'transition_identity' => 'publishing_to_promotion', 'resulting_state_identity' => 'promotion' ],
                ['type_identity' => 'publishing', 'label' => 'Unpromote', 'identity' => 'unpromote', 'initial_state_identity' => 'promotion', 'event_identity' => 'unpromote', 'transition_identity' => 'promotion_to_publishing', 'resulting_state_identity' => 'publishing' ],
                ['type_identity' => 'publishing', 'label' => 'Unpublishing', 'identity' => 'unpublishing', 'initial_state_identity' => 'publishing', 'event_identity' => 'unpublish', 'transition_identity' => 'publishing_to_unpublishing', 'resulting_state_identity' => 'unpublishing' ],
                ['type_identity' => 'publishing', 'label' => 'Feedback', 'identity' => 'feedback', 'initial_state_identity' => 'unpublishing', 'event_identity' => 'feedback', 'transition_identity' => 'unpublishing_to_feedback', 'resulting_state_identity' => 'feedback' ],
                ['type_identity' => 'publishing', 'label' => 'Archiving', 'identity' => 'archiving', 'initial_state_identity' => 'feedback', 'event_identity' => 'archiving', 'transition_identity' => 'feedback_to_archiving', 'resulting_state_identity' => 'archive' ],
            ],
            'activity_types' => [
                ['label' => 'Email', 'identity' => 'send_email'],
                ['label' => 'Sms', 'identity' => 'send_sms'],
                ['label' => 'Add worker', 'identity' => 'worker_add'],
                ['label' => 'Remove worker', 'identity' => 'worker_remove'],
            ],
            'activities' => [
                //['matrix_identity' => 'publishing', 'type_identity' => 'send_email', 'table_name' => 'dista_matrices', 'table_identity' => '', 'is_at_begin' => true, 'is_at_end' => false]
            ],

        ];

        $args = [];
        foreach( $datas as $key => $data )
        {
            $mth = '_create_' . $key;

            $this->$mth($data, $args);
        }
    }

    private function _create_matrix_types(array $data, array &$args)
    {
        $model = MatrixType::updateOrCreate(
            ['identity' => $data['identity']],
            $data
        );

        $args['matrix_type'] = $model;
    }

    private function _create_state_types(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $model = StateType::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['state_types'][$model->identity] = $model;
        }
    }

    private function _create_states(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $type_identity = $data['type_identity'];
            $type = $args['state_types'][$type_identity];

            $data['type_id'] = $type->id;

            unset( $data['type_identity']);

            $model = State::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['states'][$model->identity] = $model;
        }
    }

    private function _create_transitions(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $model = Transition::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['transitions'][$model->identity] = $model;
        }
    }

    private function _create_events(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $model = Event::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['events'][$model->identity] = $model;
        }
    }

    private function _create_workers(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $model = Worker::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['workers'][$model->identity] = $model;
        }
    }

    private function _create_event_worker_permissions(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $worker_identity = $data['worker_identity'];
            $event_identity = $data['event_identity'];

            $worker = $args['workers'][$worker_identity];
            $event = $args['events'][$event_identity];

            $data['worker_id'] = $worker->id;
            $data['event_id'] = $event->id;

            unset($data['worker_identity']);
            unset($data['event_identity']);

            $model = EventWorkerPermission::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['event_worker_permissions'][$model->identity] = $model;
        }
    }

    private function _create_matrix(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            if (array_key_exists('step_identity', $data))
                $step_identity = $data['step_identity'];
            $initial_state_identity = $data['initial_state_identity'];
            $event_identity = $data['event_identity'];
            $transition_identity = $data['transition_identity'];
            $resulting_state_identity = $data['resulting_state_identity'];

            $type = $args['matrix_type'];
            if (array_key_exists('step_identity', $data))
                $step = $args['steps'][$step_identity];
            $initial_state = $args['states'][$initial_state_identity];
            $event = $args['events'][$event_identity];
            $transition = $args['transitions'][$transition_identity];
            $resulting_state = $args['states'][$resulting_state_identity];

            $data['type_id'] = $type->id;
            if (isset($step))
                $data['step_id'] = $step->id;
            $data['initial_state_id'] = $initial_state->id;
            $data['event_id'] = $event->id;
            $data['transition_id'] = $transition->id;
            $data['resulting_state_id'] = $resulting_state->id;

            unset($data['type_identity']);
            unset($data['step_identity']);
            unset($data['initial_state_identity']);
            unset($data['event_identity']);
            unset($data['transition_identity']);
            unset($data['resulting_state_identity']);

            $model = Matrix::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['matrix'][$model->identity] = $model;
        }
    }

    private function _create_activity_types(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $model = ActivityType::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['activity_types'][$model->identity] = $model;
        }
    }

    private function _create_activities(array $datas, array &$args)
    {
        foreach( $datas as $data )
        {
            $model = ActivityType::updateOrCreate(
                ['identity' => $data['identity']],
                $data
            );

            $args['activity_types'][$model->identity] = $model;
        }
    }
}
