<?php


namespace Dendev\Dista\Traits;


use App\Traits\Model;

trait  UtilService
{
    /**
     * Crée une instance du model si l'argument est un id
     *
     * @param mixed $id_or_model id ou model
     * @param string $classname nom de la classe à instancié
     * @return Model retourne un objet de type model
     */
    private function _instantiate_if_id($id_or_model, $classname, $refresh_if_model = false)
    {
        $model = false;

        if( is_object($id_or_model) && get_class($id_or_model) == $classname )
        {
            $model = $id_or_model;
            if( $refresh_if_model )
                $model->refresh();
        }
        else if( is_object($id_or_model) )
        {
            $model = false;
            \Log::error("[UtilService:_instantiate_if_id] USiii00: Echec instantiation du model '$classname'. L'argument est un object, il devrait etre l'id ou le model lui mm", [
                    'arg' => json_encode($id_or_model,true),
                    'classe' => $classname,
                ]);
        }
        else if( is_array($id_or_model))
        {
            $model = false;
             \Log::error("[UtilService:_instantiate_if_id] USiii00: Echec instantiation du model '$classname'. L'argument est un tableau, il devrait etre l'id ou le model lui mm", [
                    'arg' => implode(',', $id_or_model),
                    'classe' => $classname,
                ]);
        }
        else
        {
            $model = $classname::find($id_or_model);
            if ( is_null($model) || ! $model )
            {
                \Log::error("[UtilService:_instantiate_if_id] USiii00: Echec instantiation du model '$classname'. L'id '$id_or_model' n'est pas bon", [
                    'id' => $id_or_model,
                    'classe' => $classname,
                ]);
            }
        }

        return $model;
    }

    private function _get_or_create_by_identity($identity, $classname )
    {
        $already_exist = $classname::where('identity', $identity)->first();
        if( $already_exist )
        {
            $model = $already_exist;
        }
        else
        {
            $model = new $classname();
            $model->identity = $identity;
        }

        return $model;
    }
}
