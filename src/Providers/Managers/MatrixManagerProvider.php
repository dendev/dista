<?php

namespace Dendev\Dista\Providers\Managers;

use Dendev\Dista\Services\Managers\MatrixManagerService;
use Illuminate\Support\ServiceProvider;

class MatrixManagerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton( 'matrix_manager', function ($app) {
            return new MatrixManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
