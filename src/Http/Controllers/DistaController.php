<?php

namespace Dendev\Dista\Http\Controllers;

use Dendev\Dista\Models\Event;
use Dendev\Dista\Models\MatrixType;
use Dendev\Dista\Models\State;
use Dendev\Dista\Models\Transition;
use Dendev\Dista\Models\Worker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class DistaController extends Controller
{
    public function sandbox_reset(Request $request)
    {
        $matrix_type_identity = $request->input('matrix_type');

        $request->session()->invalidate();

        $url = route('dista.sanbox',['matrix_type' => $matrix_type_identity]) . '#focus';
        return Redirect::to($url);
    }

    public function sandbox_resetone(Request $request)
    {
        $matrix_type_identity = $request->input('matrix_type');

        $experiments = $this->__reset_current_experiment($request);

        $url = route('dista.sanbox',['matrix_type' => $matrix_type_identity]) . '#focus';
        return Redirect::to($url);
    }

    /**
     * Display the user's profile form.
     */
    public function sandbox(Request $request, string $matrix_type)
    {
        $matrix_type = MatrixType::where('identity', $matrix_type)->first();

        if( is_null($matrix_type))
            abort(404);

        $experiments = $request->session()->get('experiments', []);

        if( count( $experiments ) == 0 )
        {
            $initial_states = \MatrixManager::get_initial_states('publishing');
            $workers = \MatrixManager::get_workers('publishing');

            $experiments[] = [
                'type' => 'state',
                'has_focus' => true,
                'is_last' => true,
                'options' => [
                    'initial_states' => $initial_states,
                    'workers' => $workers
                ],
                'inputs' => [
                    'initial_state' => $initial_states->first()
                ]
            ];
        }

        $request->session()->put('experiments', $experiments);

        return view('dendev::sanboxes.publishing', [
            'matrix_type' => $matrix_type,
            'experiments' => $experiments,
        ]);
    }

    public function execute(Request $request)
    {
        $matrix_type_identity = $request->input('matrix_type_identity');
        $matrix_type_identity = 'publishing';

        $this->_set_state($request, $matrix_type_identity);
        $this->_set_event($request, $matrix_type_identity);
        $this->_set_transition($request, $matrix_type_identity);
        $this->_set_resulting_state($request, $matrix_type_identity);

        $url = route('dista.sanbox',['matrix_type' => $matrix_type_identity]) . '#focus';
        return Redirect::to($url);
    }

    private function _set_state(Request $request, string $matrix_type_identity)
    {
        $experiment = $this->__get_current_experiment($request);


        if( $experiment['type'] === 'state')
        {
            $worker_id = $request->input('worker_id');
            $initial_state_id = $request->input('initial_state_id');

            $worker = Worker::find($worker_id);
            $initial_state = State::find( $initial_state_id);

            if( $worker && $initial_state )
            {
                $available_events = \MatrixManager::get_available_events_for_worker_and_state($worker_id, $initial_state_id, $matrix_type_identity);

                $experiment['type'] = 'event';
                $experiment['options']['events'] = $available_events;
                $experiment['inputs'] = [
                    'initial_state' => $initial_state,
                    'worker' => $worker
                ];

                $this->__update_current_experiment($request, $experiment);
            }
            else
            {
                Log::error('[Dista::DistaController::_do_events] DCde01 : Unable to instanciate model worker or model state with id in args', [
                    'worker_id' => $worker_id,
                    'initial_sate_id' => $initial_state_id
                ]);
            }

        }

        return $experiment;
    }

    private function _set_event(Request $request, string $matrix_type_identity)
    {
        $experiment = $this->__get_current_experiment($request);

        if ($experiment['type'] === 'event')
        {
            $event_id = $request->input('event_id');
            $event = Event::find($event_id);

            $initial_state = $experiment['inputs']['initial_state'];

            if ($event && $initial_state)
            {
                $transition = \MatrixManager::get_transition_for_event_and_state($event, $initial_state, $matrix_type_identity);

                $experiment['type'] = 'transition';
                $experiment['options']['transition'] = $transition;
                $experiment['inputs']['event'] = $event;

                $this->__update_current_experiment($request, $experiment);
            }
            else
            {
                Log::error('[Dista::DistaController::_set_event] DCse01 : Unable to instantiate event or get model state in session', []);
            }
        }
    }

    private function _set_transition(Request $request, string $matrix_type_identity)
    {
        $experiment = $this->__get_current_experiment($request);

        if ($experiment['type'] === 'transition')
        {
            $transition_id = $request->input('transition_id');
            $transition = Transition::find($transition_id);

            $initial_state = $experiment['inputs']['initial_state'];

            if ($transition )
            {
                $resulting_state = \MatrixManager::get_resulting_state_for_transition($transition, $matrix_type_identity);

                $experiment['type'] = 'resulting_state';
                $experiment['options']['resulting_state'] = $resulting_state;
                $experiment['inputs']['transition'] = $transition;

                $this->__update_current_experiment($request, $experiment);
            }
            else
            {
                Log::error('[Dista::DistaController::_set_trantision] DCst01 : Unable to instantiate event or get model state in session', []);
            }
        }
    }

    private function _set_resulting_state(Request $request, string $matrix_type_identity)
    {
        $experiment = $this->__get_current_experiment($request);

        if ($experiment['type'] === 'resulting_state')
        {
            $resulting_state_id = $request->input('resulting_state_id');
            $resulting_state = State::find($resulting_state_id);

            $transition = $experiment['inputs']['transition'];

            if ($resulting_state )
            {
                $resulting_state = \MatrixManager::get_resulting_state_for_transition($transition, $matrix_type_identity);

                $experiment['type'] = 'done';
                $experiment['inputs']['resulting_state'] = $resulting_state;

                //TODO add new experiments
                $this->__update_current_experiment($request, $experiment);
                $this->__update_experiments($request);
            }
            else
            {
                Log::error('[Dista::DistaController::_set_trantision] DCst01 : Unable to instantiate event or get model state in session', []);
            }
        }
    }

    private function __get_current_experiment(Request $request): array
    {
        $experiments = $request->session()->get('experiments');

        if( count($experiments ) <= 0 )
            $experiments[] = [];

        $last_index = array_key_last($experiments);

        return $experiments[$last_index];
    }

    private function __update_current_experiment(Request $request, array $experiment): void
    {
        $experiments = $request->session()->get('experiments');
        $last_index = array_key_last($experiments);
        $experiments[$last_index] = $experiment;

        $request->session()->put('experiments', $experiments);
    }

    private function __update_experiments(Request $request): void
    {
        $experiments = $request->session()->get('experiments');
        $last_index = array_key_last($experiments);
        $experiment = $experiments[$last_index];

        // clean focus && is_last
        foreach( $experiments as $key => $e)
        {
            $experiments[$key]['has_focus'] = false;
            $experiments[$key]['is_last'] = false;

        }

        // values for new
        $inital_state = $experiment['inputs']['resulting_state'];
        $worker = $experiment['inputs']['worker'];

        $initial_states = \MatrixManager::get_initial_states('publishing');
        $workers = \MatrixManager::get_workers('publishing');

        $experiment = [
            'type' => 'state',
            'has_focus' => true,
            'is_last' => true,
            'options' => [
                'workers' => $workers,
                'initial_states' => $initial_states,
            ],
            'inputs' => [
                'initial_state' => $inital_state,
                'worker' => $worker
            ]
        ];

        $experiments[] = $experiment;

        $request->session()->put('experiments', $experiments);
    }

    private function  __reset_current_experiment(Request $request)
    {
        $experiments = $request->session()->get('experiments');
        $last_index = array_key_last($experiments);
        $experiment = $experiments[$last_index];

        // values for new
        $inital_state = $experiment['inputs']['initial_state'];

        $initial_states = \MatrixManager::get_initial_states('publishing');
        $workers = \MatrixManager::get_workers('publishing');

        $experiment = [
            'type' => 'state',
            'has_focus' => true,
            'is_last' => true,
            'options' => [
                'workers' => $workers,
                'initial_states' => $initial_states,
            ],
            'inputs' => [
                'initial_state' => $inital_state
            ]
        ];

        $experiments[$last_index] = $experiment;

        $request->session()->put('experiments', $experiments);
    }
}
