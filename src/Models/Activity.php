<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'dista_activities';

    protected $fillable = [
        'workflow_id',
        'type_id',
        'table_name',
        'table_id',
        'is_at_begin',
        'is_at_end',
    ];
}
