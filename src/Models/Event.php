<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'dista_events';

    protected $fillable = [
        'label',
        'identity',
        'description',
    ];
}
