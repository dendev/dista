<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transition extends Model
{
    protected $table = 'dista_transitions';

    protected $fillable = [
        'label',
        'identity',
        'description',
    ];
}
