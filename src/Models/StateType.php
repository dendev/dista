<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StateType extends Model
{
    protected $table = 'dista_state_types';

    protected $fillable = [
        'label',
        'identity',
        'description',
    ];
}
