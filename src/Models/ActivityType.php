<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    protected $table = 'dista_activity_types';

    protected $fillable = [
        'label',
        'identity',
        'description',
    ];
}
