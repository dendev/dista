<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatrixType extends Model
{
    protected $table = 'dista_matrix_types';

    protected $fillable = [
        'label',
        'identity',
        'description',
    ];
}
