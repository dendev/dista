<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{
    protected $table = 'dista_matrices';

    protected $fillable = [
        'label',
        'identity',
        'type_id',
        'step_id',
        'initial_state_id',
        'event_id',
        'transition_id',
        'resulting_sate_id'
    ];
}
