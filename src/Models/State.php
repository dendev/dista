<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'dista_states';

    protected $fillable = [
        'type_id',
        'label',
        'identity',
        'description',
    ];
}
