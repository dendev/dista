<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventWorkerPermission extends Model
{
    protected $table = 'dista_event_worker_permissions';

    protected $fillable = [
        'event_id',
        'worker_id',
        'label',
        'identity',
        'description',
        'counter',
        'begin_at',
        'end_at',
    ];
}
