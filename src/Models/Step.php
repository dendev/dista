<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $table = 'dista_steps';

    protected $fillable = [
        'step_id',
        'workflow_id',
    ];
}
