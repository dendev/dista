<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StepType extends Model
{
    protected $table = 'dista_step_types';

    protected $fillable = [
        'label',
        'identity',
        'description',
        'order',
    ];
}
