<?php

namespace Dendev\Dista\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $table = 'dista_workers';

    protected $fillable = [
        'label',
        'identity',
        'description',
        'user_id',
    ];
}
