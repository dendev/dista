<?php

namespace Dendev\Dista\Services\Managers;

use Dendev\Dista\Models\Event;
use Dendev\Dista\Models\State;
use Dendev\Dista\Models\Transition;
use Dendev\Dista\Models\Worker;
use Dendev\Dista\Traits\UtilService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MatrixManagerService
{
    use UtilService;

    public function test_me(): string
    {
        return 'matrix_manager';
    }

    public function get_available_events_for_worker_and_state(mixed $worker_model_or_id, mixed $initial_state_model_or_id, string $matrix_type_identity): Collection
    {
        $worker = $this->_instantiate_if_id($worker_model_or_id, Worker::class);
        $initial_state = $this->_instantiate_if_id($initial_state_model_or_id, State::class);

        $sql = sprintf("
            SELECT de.*
            FROM dista_matrices dm
            INNER JOIN dista_events de ON de.id = dm.event_id
            INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
            INNER JOIN dista_event_worker_permissions dewp ON dewp.event_id = dm.event_id
            WHERE dmt.identity  = %s
            AND dm.initial_state_id = %d
            AND dewp.worker_id  = %d
            ", DB::getPdo()->quote($matrix_type_identity),
            $initial_state->id,
            $worker->id,
        );

        return Event::fromQuery($sql);
    }

    public function get_events(string $matrix_type_identity): Collection
    {
        $sql = sprintf("
            SELECT de.*
            FROM dista_events de
            INNER JOIN (
                SELECT dm.event_id
                FROM dista_matrices dm
                INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                WHERE dmt.identity = %s ) as dme on dme.event_id = de.id
            ", DB::getPdo()->quote($matrix_type_identity));

        return Event::fromQuery($sql);
    }



    public function get_initial_states(string $matrix_type_identity): Collection
    {
        $sql = sprintf("
            SELECT ds.*
            FROM dista_states ds
            INNER JOIN (
                SELECT dm.initial_state_id
                FROM dista_matrices dm
                INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                WHERE dmt.identity  = %s) as dms on dms.initial_state_id = ds.id
            ", DB::getPdo()->quote($matrix_type_identity));

        return State::fromQuery($sql);
    }

    public function get_resulting_states(string $matrix_type_identity): Collection
    {
        $sql = sprintf("
            SELECT ds.*
            FROM dista_states ds
            INNER JOIN (
                SELECT dm.resulting_state_id
                FROM dista_matrices dm
                INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                WHERE dmt.identity  = %s) as dms on dms.resulting_state_id = ds.id
            ", DB::getPdo()->quote($matrix_type_identity));

        return State::fromQuery($sql);
    }

    public function get_resulting_state_for_transition(mixed $id_or_model_transition, string $matrix_type_identity): ?State
    {
        $state = null;
        $transition = $this->_instantiate_if_id($id_or_model_transition, Transition::class);

        if( $transition )
        {
            $sql = sprintf("
            SELECT ds.*
            FROM dista_states ds
            INNER JOIN (
                SELECT dm.resulting_state_id
                FROM dista_matrices dm
                INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                WHERE dmt.identity  = %s
                AND dm.transition_id = %d) as dms on dms.resulting_state_id = ds.id
            ", DB::getPdo()->quote($matrix_type_identity),
                $transition->id,
            );

            $states = State::fromQuery($sql);

            if( ! $states->isEmpty() )
                $state = $states->first();
        }
        else
        {
            Log::error("[Dista::MatrixManagerService::get_resulting_state_for_transition] Transition model not found");
        }

        return $state;
    }

    public function get_transitions(string $matrix_type_identity): Collection
    {
        $sql = sprintf("
            SELECT dt.*
            FROM dista_transitions dt
            INNER JOIN (
                SELECT dm.transition_id
                FROM dista_matrices dm
                INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                WHERE dmt.identity  = %s) as dms on dms.transition_id = dt.id
            ", DB::getPdo()->quote($matrix_type_identity));

        return Transition::fromQuery($sql);
    }

    public function get_transition_for_event_and_state(mixed $event_id_or_model, mixed $state_id_or_model, string $matrix_type_identity): ?Transition
    {
        $transition = null;

        $event = $this->_instantiate_if_id($event_id_or_model, Event::class);
        $state = $this->_instantiate_if_id($state_id_or_model, State::class);

        if( $event && $state )
        {
            $sql = sprintf("
            SELECT dt.*
            FROM dista_transitions dt
            WHERE dt.id = (
                SELECT dm.transition_id
                FROM dista_matrices dm
                INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                WHERE dmt.identity  = %s
                AND dm.initial_state_id = %d
                AND dm.event_id = %d
                )
            ",
                DB::getPdo()->quote($matrix_type_identity),
                intval($state->id),
                intval($event->id),
            );

            $transition = Transition::fromQuery($sql);

            if( $transition->isEmpty() )
                $transition = null;
            else
                $transition = $transition->first();
        }
        else
        {
            Log::error('[DISTA::MatrixManagerService::get_transition_for_event_and_states] MMSgtfeas01: Missing event or state model. Check your args.');
        }

        return $transition;
    }

    public function get_workers(string $matrix_type_identity): Collection
    {
        $sql = sprintf("
                SELECT DISTINCT  dw.*
                FROM dista_events de
                INNER JOIN dista_event_worker_permissions dewp on dewp.event_id = de.id
                INNER JOIN dista_workers dw on dw.id = dewp.worker_id
                INNER JOIN (
                    SELECT dm.event_id
                    FROM dista_matrices dm
                    INNER JOIN dista_matrix_types dmt ON dmt.id  = dm.type_id
                    WHERE dmt.identity = %s ) as dme on dme.event_id = de.id
            ", DB::getPdo()->quote($matrix_type_identity));

        return Worker::fromQuery($sql);
    }
}
