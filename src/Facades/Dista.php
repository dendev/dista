<?php

namespace Dendev\Dista\Facades;

use Illuminate\Support\Facades\Facade;

class Dista extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'dista';
    }
}
