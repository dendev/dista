<?php
namespace Dendev\Dista\Facades\Managers;

use Illuminate\Support\Facades\Facade;

class MatrixManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'matrix_manager';
    }
}
