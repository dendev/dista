<?php

namespace Dendev\Dista;

use Dendev\Dista\Facades\Managers\MatrixManagerFacade;
use Dendev\Dista\Providers\Managers\MatrixManagerProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class DistaServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'dendev');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'dendev');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/dista.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/dista.php', 'dista');

        // Register the service the package provides.
        $this->app->singleton('dista', function ($app) {
            return new Dista;
        });

        // services
        $this->app->register(MatrixManagerProvider::class);

        // facades
        $loader = AliasLoader::getInstance();
        $loader->alias('MatrixManager', MatrixManagerFacade::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['dista'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/dista.php' => config_path('dista.php'),
        ], 'dista.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/dendev'),
        ], 'dista.views');*/

        // Publishing assets.
        $this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/dendev'),
        ], 'dista.assets');

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/dendev'),
        ], 'dista.lang');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
